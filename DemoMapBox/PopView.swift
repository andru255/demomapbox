//
//  Bubble.swift
//  TappyTap
//
//  Created by Ben Lambert on 11/13/17.
//  Copyright © 2017 collectiveidea. All rights reserved.
//

import UIKit

class Bubble: CAShapeLayer, CAAnimationDelegate {
    
    override init() {
        super.init()
        
        addCircle()
    }
    
    override init(layer: Any) {
        super.init(layer: layer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addCircle() {
        let circlePath = UIBezierPath(ovalIn: CGRect(x: -15, y: -15, width: 30, height: 30))
        
        path = circlePath.cgPath
        strokeColor = UIColor.darkGray.cgColor
        fillColor = UIColor.clear.cgColor
        lineWidth = 1
    }
    
    func animate() {
        let scaleAnim = CABasicAnimation(keyPath: "transform.scale")
        scaleAnim.fromValue = 1
        scaleAnim.toValue = 1.25
        scaleAnim.duration = 0.1
        
        scaleAnim.delegate = self
        
        add(scaleAnim, forKey: "scaleCircle")
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag {
            CATransaction.begin()
            CATransaction.setDisableActions(true)
            opacity = 0
            CATransaction.commit()
        }
    }
    
}

//
//  Line.swift
//  TappyTap
//
//  Created by Ben Lambert on 11/13/17.
//  Copyright © 2017 collectiveidea. All rights reserved.
//

import UIKit

class Line: CAShapeLayer {
    
    override init() {
        super.init()
        
        createLine()
    }
    
    override init(layer: Any) {
        super.init(layer: layer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createLine() {
        let bezPath = UIBezierPath()
        bezPath.move(to: CGPoint(x: 15, y: 0))
        let distance = CGFloat(arc4random_uniform(45 - 25) + 25)
        bezPath.addLine(to: CGPoint(x: distance, y: 0))
        
        lineWidth = 2
        lineCap = CAShapeLayerLineCap.round
        strokeColor = UIColor.darkGray.cgColor
        path = bezPath.cgPath
    }
    
    func animate() {
        let duration: CFTimeInterval = 0.6
        
        let end = CABasicAnimation(keyPath: "strokeEnd")
        end.fromValue = 0
        end.toValue = 1.0175
        end.beginTime = 0
        end.duration = duration * 0.75
        end.timingFunction = CAMediaTimingFunction(controlPoints: 0.2, 0.88, 0.09, 0.99)
        end.fillMode = CAMediaTimingFillMode.forwards
        
        let begin = CABasicAnimation(keyPath: "strokeStart")
        begin.fromValue = 0
        begin.toValue = 1.0175
        begin.beginTime = duration * 0.15
        begin.duration = duration * 0.85
        begin.timingFunction = CAMediaTimingFunction(controlPoints: 0.2, 0.88, 0.09, 0.99)
        begin.fillMode = CAMediaTimingFillMode.backwards
        
        let group = CAAnimationGroup()
        group.animations = [end, begin]
        group.duration = duration
        
        strokeEnd = 1
        strokeStart = 1
        
        add(group, forKey: "move")
    }
    
}


//
//  PopView.swift
//  DemoMapBox
//
//  Created by andru on 10/11/18.
//  Copyright © 2018 El Comercio. All rights reserved.
//

import UIKit

class PopView: UIView {
    
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        
        isUserInteractionEnabled = false
        
        let bubble = Bubble()
        bubble.position = CGPoint(x: frame.width / 2, y: frame.height / 2)
        layer.addSublayer(bubble)
        bubble.animate()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.07, execute: {
            for number in 1...6 {
                let line = Line()
                line.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
                line.transform = CATransform3DMakeRotation(CGFloat.pi * 2 / CGFloat(6) * CGFloat(number), 0, 0, 1)
                self.layer.addSublayer(line)
                line.animate()
            }
        })
        
        let minOffset: UInt32 = 0
        let maxOffset: UInt32 = 200
        let rotation = CGFloat(arc4random_uniform(maxOffset - minOffset) + minOffset) / CGFloat(100)
        transform = CGAffineTransform(rotationAngle: rotation)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
