//
//  MapRender.swift
//  DemoMapBox
//
//  Created by andru on 10/4/18.
//  Copyright © 2018 El Comercio. All rights reserved.
//
import Mapbox
import UIKit

// MGLAnnotation protocol reimplementation

class CustomAnnotationView: MGLAnnotationView {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Use CALayer’s corner radius to turn this view into a circle.
        layer.cornerRadius = bounds.width / 2
        layer.borderWidth = 2
        layer.borderColor = UIColor.white.cgColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Animate the border width in/out, creating an iris effect.
        /*let animation = CABasicAnimation(keyPath: "transform.scale.y")
        animation.duration = 0.3
        animation.fromValue = 1
        animation.toValue = 3
        layer.borderWidth = selected ? bounds.width / 4 : 2
        layer.add(animation, forKey: "borderWidth")*/
    }
}

// MGLPolyline subclass
class CustomPolyline: MGLPolyline {
    // Because this is a subclass of MGLPolyline, there is no need to redeclare its properties.
    
    // Custom property that we will use when drawing the polyline.
    var color: UIColor?
}

class MapRenderer {
    private var view: UIView!
    private var target: MGLMapView!
    private var position: CLLocationCoordinate2D!
    private var type: Int!
    private var styleURL: URL? = URL(string: "mapbox://styles/mapbox/streets-v9")
    weak var delegate: MGLMapViewDelegate?

    enum LocationType {
        case point
        case aprox
        case none
    }

    init(view: UIView, position: CLLocationCoordinate2D, type: Int = 0) {
        self.view = view
        self.position = position
        self.type = type
    }

    func checkLocation(with type: Int) -> LocationType {
        switch type {
        case 1: return .point
        case 2: return .aprox
        default: return .none
        }
    }

    func render() {
        switch checkLocation(with: self.type) {
        case .point:
            createAndAppendMapView()
            self.target.setCenter(position, zoomLevel: 16, animated: true)
            self.target.styleURL = styleURL
            if let shapeCollection = getGeoJsonPoint() as? MGLShapeCollectionFeature,
                let annotation = shapeCollection.shapes.first {
                let ca = MGLPointAnnotation()
                ca.coordinate = annotation.coordinate
                self.target.addAnnotation(ca)
            }

        case .aprox:
            createAndAppendMapView()
            self.target.setCenter(position, zoomLevel: 14, animated: true)
            let circle = circlePath(center: position, distance: 300)
            let shape = MGLPolygon(coordinates: circle, count: UInt(circle.count))
            self.target.addAnnotation(shape)

        default:
            return
        }
    }

    func getMapInstance() -> MGLMapView? {
        return self.target ?? nil
    }

    private func dot(size: Int) -> UIImage {
        let floatSize = CGFloat(size)
        let rect = CGRect(x: 0, y: 0, width: floatSize, height: floatSize)
        let strokeWidth: CGFloat = 1
        
        UIGraphicsBeginImageContextWithOptions(rect.size, false, UIScreen.main.scale)
        
        let ovalPath = UIBezierPath(ovalIn: rect.insetBy(dx: strokeWidth, dy: strokeWidth))
        let color = UIColor.withAlphaComponent(UIColor.darkGray)(0.5)
        color.setFill()
        ovalPath.fill()

        UIColor.white.setStroke()
        ovalPath.lineWidth = strokeWidth
        ovalPath.stroke()

        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()

        return image
    }

    private func createAndAppendMapView() {
        self.target = MGLMapView(frame: self.view.frame)
        target.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        view.addSubview(self.target)
        target.delegate = delegate
        self.target.isUserInteractionEnabled = false

        // constraints
        self.target.translatesAutoresizingMaskIntoConstraints = false
        self.target.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.target.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.target.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.target.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.target.layoutIfNeeded()
    }

    private func getGeoJsonPoint() ->  MGLShape? {
        let colorMarker = #colorLiteral(red: 0.5490000248, green: 0.7059999704, blue: 0.125, alpha: 1).hexString
        let metaData: [String: Any] = [
            "type": "FeatureCollection",
            "features": [[
                "type": "Feature",
                "properties": [
                    "marker-color": colorMarker,
                    "marker-size": "medium",
                    "marker-symbol": ""
                ],
                "geometry": [
                    "type": "Point",
                    "coordinates": [self.position.longitude, self.position.latitude]
                ]
                ]]
        ]
        guard let data = try? JSONSerialization.data(withJSONObject: metaData, options: []),
            let shape = try? MGLShape(data: data, encoding: String.Encoding.utf8.rawValue) else {
            return nil
        }
        return shape
    }

    private func circlePath(center: CLLocationCoordinate2D, distance: Double) -> [CLLocationCoordinate2D] {
        var result: [CLLocationCoordinate2D] = []
        let steps = 64
        let metters = distance / 1000
        let distanceX = metters / (111.320 * cos(center.latitude * Double.pi/180))
        let distanceY = metters / 110.574
        for angle in 0..<steps {
            let radianAngle = Double(angle)/Double(steps) * (2 * Double.pi)
            let x = center.longitude + (distanceX * cos(radianAngle))
            let y = center.latitude + (distanceY * sin(radianAngle))
            result.append(CLLocationCoordinate2D(latitude: y, longitude: x))
        }
        result.append(result[0])
        return result
    }
}
