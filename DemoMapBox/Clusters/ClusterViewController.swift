//
//  ClusterViewController.swift
//  DemoMapBox
//
//  Created by andru on 2/11/19.
//  Copyright © 2019 El Comercio. All rights reserved.
//

import Mapbox
import UIKit

typealias JSONValue = Any
typealias JSONObject = [String: JSONValue]

struct RequestData: Codable {
    var points: [PointData]
}

struct PointData: Codable {
    var id: String
    var latitude: String
    var longitude: String
    var size: Int
    var type: String
    var publicationType: Int
}

class ClusterViewController: UIViewController {

    @IBOutlet weak var containerMapView: UIView!
    var targetMap: MGLMapView!
    let center = CLLocationCoordinate2D(latitude: -12.05, longitude: -77.04719692468645)

    var clusterAnnotations: [ClusterAnnotation] = []
    var annotations: [AnnouncementAnnotation] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        createAndAppendMapView(view: containerMapView)
        setupGestureEvent()
    }

    @IBAction func onReload(_ sender: UIButton) {
        let zoom = Int(targetMap.zoomLevel)
        print("zoom", zoom)
        print("bounds", targetMap.bounds)
        let zoomDemo = [13, 16].randomElement()!

        fetch(zoom: zoomDemo) { (points) in
            clusterAnnotations.forEach({ clusterAnnotation in
                clusterAnnotation.removeSourcesAndStyles(style: targetMap.style)
            })
            annotations.forEach({ annotation in
                annotation.removeSourcesAndStyles(style: targetMap.style)
            })
            updateAnnotations(mapView: targetMap, points: points)
        }
    }
}
