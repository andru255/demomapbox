//
//  ClusterViewController+UIGestureRecognizerDelegate.swift
//  DemoMapBox
//
//  Created by andru on 2/12/19.
//  Copyright © 2019 El Comercio. All rights reserved.
//

import UIKit

extension ClusterViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        // This will only get called for the custom double tap gesture,
        // that should always be recognized simultaneously.
        return true
    }

    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        // This will only get called for the custom double tap gesture.
        return firstPoint(with: gestureRecognizer) != nil
    }
}
