//
//  ClusterViewController+Tools.swift
//  DemoMapBox
//
//  Created by andru on 2/14/19.
//  Copyright © 2019 El Comercio. All rights reserved.
//

import Foundation
import Mapbox

extension ClusterViewController {
    public func updateAnnotations(mapView: MGLMapView, points: [PointData]) {
        clusterAnnotations = points.enumerated().map { (index, point)  -> ClusterAnnotation? in
            if point.type != "cluster" {
                return nil
            }
            let position = CLLocationCoordinate2D(latitude: Double(point.latitude) ?? 0, longitude: Double(point.longitude) ?? 0)
            let suffixId = "|\(index)"
            let cluster = ClusterAnnotation(position: position)
            cluster.setText(String(point.size))
            cluster.applyStyles(suffixId: suffixId, style: targetMap?.style)
            return cluster
        }.compactMap({ $0 })

        annotations = points.enumerated().map { (index, point)  -> AnnouncementAnnotation? in
            if point.type == "cluster" {
                return nil
            }
            let position = CLLocationCoordinate2D(latitude: Double(point.latitude) ?? 0, longitude: Double(point.longitude) ?? 0)
            let suffixId = "|\(index)"
            // annotation
            let annotation = AnnouncementAnnotation(publicationType: point.publicationType, position: position, id: point.id)
            annotation.applyStyles(suffixId: suffixId, style: targetMap?.style)
            return annotation
        }.compactMap({ $0 })
    }

    public func getLayerIds() -> [String] {
        var result: [String] = []
        result += clusterAnnotations.map({ (item) -> String in
            return item.getLayerId()
        })
        result += annotations.map({ (item) -> String in
            return item.getLayerId()
        })
        return result
    }
}
