//
//  ClusterViewController+Setup.swift
//  DemoMapBox
//
//  Created by andru on 2/12/19.
//  Copyright © 2019 El Comercio. All rights reserved.
//

import Foundation
import Mapbox
import UIKit

extension ClusterViewController {
    func createAndAppendMapView(view: UIView) {
        targetMap = MGLMapView(frame: view.frame)
        targetMap.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        view.addSubview(targetMap)
        targetMap.delegate = self

        // constraints
        targetMap.translatesAutoresizingMaskIntoConstraints = false
        targetMap.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        targetMap.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        targetMap.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        targetMap.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        targetMap.layoutIfNeeded()

        // default setup
        targetMap.setCenter(center, zoomLevel: 12, animated: true)
        targetMap.styleURL = URL(string: "mapbox://styles/mapbox/streets-v9")
    }

    func setupGestureEvent() {
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapCluster(sender:)))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.delegate = self

        // It's important that this new double tap fails before the map view's
        // built-in gesture can be recognized. This is to prevent the map's gesture from
        // overriding this new gesture (and then not detecting a cluster that had been
        // tapped on).
        for recognizer in targetMap.gestureRecognizers!
            where (recognizer as? UITapGestureRecognizer)?.numberOfTapsRequired == 2 {
            recognizer.require(toFail: doubleTap)
        }
        targetMap.addGestureRecognizer(doubleTap)

        // Add a single tap gesture recognizer. This gesture requires the built-in
        // MGLMapView tap gestures (such as those for zoom and annotation selection)
        // to fail (this order differs from the double tap above).
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(handleMapTap(sender:)))
        for recognizer in targetMap.gestureRecognizers!
            where recognizer is UITapGestureRecognizer {
            singleTap.require(toFail: recognizer)
        }
        targetMap.addGestureRecognizer(singleTap)

        // Handling pinch gestures
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(handleMapPinch(sender:)))
        pinch.delegate = self
        targetMap.addGestureRecognizer(pinch)
    }

    @objc func handleDoubleTapCluster(sender: UITapGestureRecognizer) {
        if let point = firstPoint(with: sender),
            let type = point.attribute(forKey: "type") as? String,
            type == "point" {
            let id = point.attribute(forKey: "id") as? String
            let alert = UIAlertController(title: "id del aviso", message: id, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "OK", style: .cancel)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }

    @objc func handleMapTap(sender: UITapGestureRecognizer) {
        print("handleMapTap")
    }

    @objc func handleMapPinch(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            print("zoom!!!!", targetMap.zoomLevel)
        }
    }

    func firstPoint(with gestureRecognizer: UIGestureRecognizer) -> MGLFeature? {
        let point = gestureRecognizer.location(in: gestureRecognizer.view)
        let features = targetMap.visibleFeatures(at: point, styleLayerIdentifiers: Set(getLayerIds()))
        return features.first
    }
}
