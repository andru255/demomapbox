//
//  ClusterViewController+Request.swift
//  DemoMapBox
//
//  Created by andru on 2/12/19.
//  Copyright © 2019 El Comercio. All rights reserved.
//
import Foundation

extension ClusterViewController {
    func fetch(zoom: Int, completion: (_ points: [PointData]) -> Void ) {
        guard let data = getData(by: "response_zoom\(zoom)") else {
            return
        }
        print("zoom", zoom, data.points.count)
        completion(data.points)
    }

    func getData(by fileName: String) -> RequestData? {
        guard let url = Bundle.main.url(forResource: fileName, withExtension: "json"),
            let data = try? Data(contentsOf: url) else {
                return nil
        }
        do {
            let decoder = JSONDecoder()
            return try decoder.decode(RequestData.self, from: data)
        } catch let error {
            print(error)
        }
        return nil
    }
}
