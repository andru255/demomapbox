//
//  ClusterAnnotation.swift
//  DemoMapBox
//
//  Created by andru on 2/13/19.
//  Copyright © 2019 El Comercio. All rights reserved.
//

import Mapbox

// custom annotations
class ClusterPolygon: MGLPolygon {
    var color: UIColor?
}

class ClusterAnnotation {
    private var center: CLLocationCoordinate2D
    private var text: String = ""
    private var suffix: String = ""
    private var idPolyFill: String = ""
    private var idPolyline: String = ""
    private var idPolylineCenter: String = ""

    init(position: CLLocationCoordinate2D) {
        self.center = position
    }

    func setText(_ content: String) {
        self.text = content
    }

    func applyStyles(suffixId: String, style: MGLStyle?) {
        guard let style = style else {
            return
        }
        self.suffix = suffixId
        self.idPolyFill = "polyfill\(suffixId)"
        self.idPolyline = "polyline\(suffixId)"
        self.idPolylineCenter = "center\(suffixId)"

        // shape
        let shapeSource = getClusterShapeSource(identifier: idPolyline, position: self.center)

        // add shape source
        style.addSource(shapeSource)
        
        // source center
        let sourceCenter = getCenterClusterShapeSource(identifier: idPolylineCenter, position: self.center)
        // add shape source
        style.addSource(sourceCenter)

        // fill style
        let fillLayer = getClusterFillStyleLayer(identifier: idPolyFill, source: shapeSource)
        // add layer
        style.addLayer(fillLayer)

        // line style
        let layer = getClusterLineStyleLayer(identifier: idPolyline, source: shapeSource)
        // add layer
        style.addLayer(layer)
        
        // text style
        let textLayer = getClusterCenterSymbolStyleLayer(identifier: idPolylineCenter, source: sourceCenter, text: self.text)
        // add text layer
        style.addLayer(textLayer)
    }

    func getLayerId() -> String {
        return self.idPolyFill
    }

    func removeSourcesAndStyles(style: MGLStyle?) {
        if  let source = style?.source(withIdentifier: self.idPolyline),
            let sourceCenter = style?.source(withIdentifier: self.idPolylineCenter),
            let styleLine = style?.layer(withIdentifier: self.idPolyline),
            let styleFill = style?.layer(withIdentifier: self.idPolyFill),
            let styleCenter = style?.layer(withIdentifier: self.idPolylineCenter) {
            style?.removeLayer(styleLine)
            style?.removeLayer(styleFill)
            style?.removeLayer(styleCenter)
            style?.removeSource(source)
            style?.removeSource(sourceCenter)
        }
    }

    private func getClusterShapeSource(identifier: String, position: CLLocationCoordinate2D) -> MGLShapeSource {
        let coords = makeCirclePath(center: position, distance: 100)
        let shapeFeature = MGLPolylineFeature(coordinates: coords, count: UInt(coords.count))
        shapeFeature.attributes = ["type": "cluster"]
        let shapeSource = MGLShapeSource(identifier: identifier, shape: shapeFeature, options: nil)
        return shapeSource
    }
    
    private func getCenterClusterShapeSource(identifier: String, position: CLLocationCoordinate2D) -> MGLShapeSource {
        let feature = MGLPointFeature()
        feature.attributes = ["type": "cluster"]
        feature.coordinate = position
        let source = MGLShapeSource(identifier: identifier, features: [feature], options: nil)
        return source
    }
    
    private func getClusterLineStyleLayer(identifier: String, source: MGLShapeSource) -> MGLLineStyleLayer {
        let layer = MGLLineStyleLayer(identifier: identifier, source: source)
        layer.lineJoin = NSExpression(forConstantValue: "round")
        layer.lineCap = NSExpression(forConstantValue: "round")
        layer.lineColor = NSExpression(forConstantValue: UIColor.gray)
        layer.lineWidth = NSExpression(format: "mgl_interpolate:withCurveType:parameters:stops:($zoomLevel, 'linear', nil, %@)", [13: 2, 14: 0])
        return layer
    }

    private func getClusterFillStyleLayer(identifier: String, source: MGLShapeSource) -> MGLFillStyleLayer {
        let layer = MGLFillStyleLayer(identifier: identifier, source: source)
        layer.fillColor = NSExpression(forConstantValue: UIColor.gray)
        layer.fillOpacity = NSExpression(forConstantValue: 0.25)
        return layer
    }

    private func getClusterCenterSymbolStyleLayer(identifier: String, source: MGLShapeSource, text: String) -> MGLSymbolStyleLayer {
        let textLayer = MGLSymbolStyleLayer(identifier: identifier, source: source)
        textLayer.text = NSExpression(format: "%@", text)
        return textLayer
    }

    private func makeCirclePath(center: CLLocationCoordinate2D, distance: Double) -> [CLLocationCoordinate2D] {
        var result: [CLLocationCoordinate2D] = []
        let steps = 64
        let metters = distance / 1000
        let distanceX = metters / (111.320 * cos(center.latitude * Double.pi/180))
        let distanceY = metters / 110.574
        for angle in 0..<steps {
            let radianAngle = Double(angle)/Double(steps) * (2 * Double.pi)
            let x = center.longitude + (distanceX * cos(radianAngle))
            let y = center.latitude + (distanceY * sin(radianAngle))
            result.append(CLLocationCoordinate2D(latitude: y, longitude: x))
        }
        result.append(result[0])
        return result
    }

    
}
