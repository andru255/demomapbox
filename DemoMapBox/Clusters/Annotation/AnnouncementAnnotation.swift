//
//  Single.swift
//  DemoMapBox
//
//  Created by andru on 2/13/19.
//  Copyright © 2019 El Comercio. All rights reserved.
//

import Mapbox

class ProjectAnnotation: MGLPointAnnotation {
}

class SingleAnnotation: MGLPointAnnotation {
}

class AnnouncementAnnotation {
    private var publicationType: Int
    private var position: CLLocationCoordinate2D
    private var id: String

    private var annotationInstance: MGLPointAnnotation!
    private var imageName: String = ""
    private var idSourceShape: String = ""
    private var idSymbol: String = ""

    init(publicationType: Int, position: CLLocationCoordinate2D, id: String) {
        self.publicationType = publicationType
        self.position = position
        self.id = id
    }

    func applyStyles(suffixId: String, style: MGLStyle?) {
        guard let style = style else {
            return
        }
        imageName = "customAnnotation\(suffixId)"
        idSourceShape = "annotation\(suffixId)"
        idSymbol = "symbolAnnotation\(suffixId)"
        var icon = #imageLiteral(resourceName: "AnnounceAnnotationMap")
        if annotationInstance is ProjectAnnotation {
            icon = #imageLiteral(resourceName: "ProyectAnnotationMap")
        }
        style.setImage(icon, forName: imageName)
        let source = getAnnotationShapeSource(identifier: idSourceShape, position: self.position)
        let symbol = getAnnotationStyleLayer(identifier: idSymbol, source: source)
        symbol.iconImageName = NSExpression(forConstantValue: imageName)
        symbol.iconScale = NSExpression(forConstantValue: 0.15)
        style.addSource(source)
        style.addLayer(symbol)
    }

    func getLayerId() -> String {
        return idSymbol
    }

    func removeSourcesAndStyles(style: MGLStyle?) {
        if  let source = style?.source(withIdentifier: idSourceShape),
            let styleLine = style?.layer(withIdentifier: idSymbol) {
            style?.removeImage(forName: imageName)
            style?.removeLayer(styleLine)
            style?.removeSource(source)
        }
    }

    func getAnnotationShapeSource(identifier: String, position: CLLocationCoordinate2D) -> MGLShapeSource {
        let point = MGLPointFeature()
        point.coordinate = position
        point.attributes = [
            "type": "point",
            "id": self.id
        ]
        let shapeSource = MGLShapeSource(identifier: identifier, features: [point], options: nil)
        return shapeSource
    }

    func getAnnotationStyleLayer(identifier: String, source: MGLShapeSource) -> MGLSymbolStyleLayer {
        let symbol = MGLSymbolStyleLayer(identifier: identifier, source: source)
        return symbol
    }
}
