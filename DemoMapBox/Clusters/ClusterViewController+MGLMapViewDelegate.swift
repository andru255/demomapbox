//
//  ClusterViewController+MGLMapViewDelegate.swift
//  DemoMapBox
//
//  Created by andru on 2/12/19.
//  Copyright © 2019 El Comercio. All rights reserved.
//

import Mapbox

extension ClusterViewController: MGLMapViewDelegate {
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
        fetch(zoom: 13) { (points) in
            updateAnnotations(mapView: mapView, points: points)
        }
    }
}
