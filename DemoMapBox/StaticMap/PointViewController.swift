//
//  PointViewController.swift
//  DemoMapBox
//
//  Created by andru on 1/29/19.
//  Copyright © 2019 El Comercio. All rights reserved.
//


import MapboxStatic
import UIKit

class PointViewController: UIViewController {

    @IBOutlet weak var containerMapView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        let position = CLLocationCoordinate2D(latitude: 40.74699, longitude: -73.98742)
        //let positionCustomMarker = CLLocationCoordinate2D(latitude: 40.74699, longitude: -73.988)
        let aprox = StaticMapRender(view: containerMapView, position: position, type: 1)
        aprox.render()
    }

}
