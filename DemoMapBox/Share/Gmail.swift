//
//  Gmail.swift
//  DemoMapBox
//
//  Created by andru on 10/16/18.
//  Copyright © 2018 El Comercio. All rights reserved.
//
import UIKit

final class GmailNewEmailShareActivity: UIActivity {
    
    private let urlScheme = "googlegmail:"
    private var activityItems: [Any]? = nil

    private var subject: String?
    private var body: Any?
    
    init(subject: String, body: Any?) {
        self.subject = subject
        self.body = body
    }

    override class var activityCategory: UIActivity.Category {
        return .share
    }
    
    override var activityType: UIActivity.ActivityType? {
        return UIActivity.ActivityType(rawValue: "googlemail")
    }
    
    override var activityTitle: String? {
        return "Gmail"
    }
    
    override var activityViewController: UIViewController? {
        return nil
    }
    
    override func canPerform(withActivityItems activityItems: [Any]) -> Bool {
        guard let url = URL(string: urlScheme) else {
            return false
        }
        return UIApplication.shared.canOpenURL(url)
    }
    
    override func prepare(withActivityItems activityItems: [Any]) {
        // At this point I store incoming activity items to use them later in `perform()`
        self.activityItems = activityItems
    }
    
    override var activityImage: UIImage? {
        // 60x60, 120x120 and 180x180
        return nil
    }
    
    override func perform() {
        guard let subject = subject,
            let encodedSubject = subject.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            else {
            activityDidFinish(false)
            return
        }
        guard let body = body as? URL else {
            activityDidFinish(false)
            return
        }
        let urlString = "\(urlScheme)///co?subject=\(encodedSubject)&body=\(body.absoluteString)"
        guard let url = URL(string: urlString) else {
            activityDidFinish(false)
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
        activityDidFinish(true)
    }
}
