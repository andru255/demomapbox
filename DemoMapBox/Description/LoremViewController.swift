//
//  LoremViewController.swift
//  DemoMapBox
//
//  Created by andru on 11/16/18.
//  Copyright © 2018 El Comercio. All rights reserved.
//

import UIKit

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

extension Range where Bound == String.Index {
    var nsRange:NSRange {
        return NSRange(location: self.lowerBound.encodedOffset,
                       length: self.upperBound.encodedOffset -
                        self.lowerBound.encodedOffset)
    }
}

class LoremViewController: UIViewController {

    @IBOutlet weak var lblResume: UILabel!
    @IBOutlet weak var txtViewResume: UITextView!
    let lorem = "¡OCASION¡¡¡¡ Se vende terreno cercado con muro perimetral, con un total de 6 lotes en el condominio. Ubicacion predio MILI, condominio MILI, ubicado en rinconada alta de Puruhuay , en el distrito de Lurin. Esta cerca a la Universidad Alas Peruanas,  Universidad San Ignacio de Loyola, Nuevo estudio America TV, Restaurantes y otros negocios de la zona. El terreno se encuentran demo@gmail.com independizado y registrado en Sunarp. Se vende con todo el proyecto para la construcción de una casa, planos estructurales, arquitectónicos, eléctricos, agua y desague."

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        lblResume.text = lorem
        txtViewResume.text = lorem
        setupTxtView()
    }

    @IBAction func tapLabel(_ sender: UITapGestureRecognizer) {
        print("tap!")
        let text = lblResume.text!
        let emailRange = (text as NSString).range(of: "demo@gmail.com")
        if sender.didTapAttributedTextInLabel(label: lblResume, inRange: emailRange) {
            print(":O")
        }
    }

    func setupTxtView() {
        let attributedString = NSMutableAttributedString(string: lorem, attributes: nil)
        attributedString.addAttribute(.link, value: "#contactForm", range: NSRange(location: 378, length: 14))
        self.txtViewResume.delegate = self
        self.txtViewResume.attributedText = attributedString
        self.txtViewResume.linkTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.red]
    }
}

extension LoremViewController: UITextViewDelegate {
    
    @available(iOS, deprecated: 10.0)
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
        return false
    }
    
    //For iOS 10
    @available(iOS 10.0, *)
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        return false
    }
}
