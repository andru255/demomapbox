//
//  ViewController.swift
//  DemoMapBox
//
//  Created by andru on 9/25/18.
//  Copyright © 2018 El Comercio. All rights reserved.
//

import UIKit
import MapboxStatic
//import Mapbox

extension String {
    func replace(pattern: String, options: NSRegularExpression.Options = [], with: String) -> String? {
        let regex = try? NSRegularExpression(pattern: pattern, options: options)
        let range = NSRange(location: 0, length: self.count)
        guard let replaced = regex?.stringByReplacingMatches(in: self, options: [], range: range, withTemplate: with) else {
            return ""
        }
        return replaced
    }
    
    func match(pattern: String) -> [String] {
        do {
            let text = self as NSString
            let regex = try NSRegularExpression(pattern: pattern)
            let range = NSRange(location: 0, length: self.count)
            return regex.matches(in: self, range: range).map { result in
                return text.substring(with: result.range)
            }
            
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
}

struct Share {
    static let title = "Mira este inmueble que encontré usando la App de Urbania.pe"
    static let subject = "Visita el inmueble en Urbania.pe"
}

class ViewController: UIViewController {

    @IBOutlet weak var mapWithMarkerView: UIView!
    @IBOutlet weak var mapWithCircle: UIView!
    @IBOutlet weak var emptyView: UIView!

    @IBOutlet weak var demoFormatNumber: UITextField!

    //weak var map: MGLMapView?

    override func viewDidLoad() {
        super.viewDidLoad()

        /*let exampleImage = UIImageView(frame: self.view.frame)
        self.view.addSubview(exampleImage)
         */
        let position = CLLocationCoordinate2D(latitude: 40.74699, longitude: -73.98742)
        let positionCustomMarker = CLLocationCoordinate2D(latitude: 40.74699, longitude: -73.988)
        let positionAprox = CLLocationCoordinate2D(latitude: 40.74699, longitude: -73.98742)

        /*
        let point = MapRenderer(view: mapWithMarkerView, position: position, type: 1)
        point.delegate = self
        
        point.render()
        map = point.getMapInstance()
        */

        /*
        let aprox = StaticMapRender(view: mapWithCircle, position: position, type: 2)
        aprox.render()
 

        // marker
        let camera = SnapshotCamera(lookingAtCenter: position, zoomLevel: 16)
        let styleURL = URL(string: "mapbox://styles/mapbox/streets-v9")
        let markerOverlay = Marker(coordinate: position, size: .medium, iconName: "cafe")

        // custom marker
        let customMarker = CustomMarker(coordinate: positionCustomMarker, url: URL(string: "http://maps.google.com/mapfiles/ms/icons/blue.png")!)

        // circle
        let circleCoords = circlePath(center: positionAprox, distance: 50)
        let circle = Path(coordinates: circleCoords)
        circle.fillColor = UIColor.gray.withAlphaComponent(0.25)
        // setup options
        let options = SnapshotOptions(styleURL: styleURL!, camera: camera, size: exampleImage.bounds.size)
        options.overlays = [markerOverlay, customMarker, circle]
        _ = Snapshot(options: options).image { (image, error)  in
            if let existError = error {
                print(existError)
                return
            }
            exampleImage.image = image
        }
        */
        // mapbox
        //let point = MapRenderer(view: mapWithMarkerView, position: position, type: 1)
        //point.render()

        //let aprox = MapRenderer(view: mapWithCircle, position: position, type: 2)
        //aprox.delegate = self
        //aprox.render()
    }

    func circlePath(center: CLLocationCoordinate2D, distance: Double) -> [CLLocationCoordinate2D] {
        var result: [CLLocationCoordinate2D] = []
        let steps = 64
        let metters = distance / 1000
        let distanceX = metters / (111.320 * cos(center.latitude * Double.pi/180))
        let distanceY = metters / 110.574
        for angle in 0..<steps {
            let radianAngle = Double(angle)/Double(steps) * (2 * Double.pi)
            let x = center.longitude + (distanceX * cos(radianAngle))
            let y = center.latitude + (distanceY * sin(radianAngle))
            result.append(CLLocationCoordinate2D(latitude: y, longitude: x))
        }
        result.append(result[0])
        return result
    }

    @IBAction func onShare(_ sender: Any) {
        var demo = "coool http://www.google.com"
        var rawUrl = "https://urbania.pe"
        //url += "/aviso-n"
        //let URLFormed = URL(string: url)
        let excludeActivity = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.print, UIActivity.ActivityType.assignToContact, UIActivity.ActivityType.saveToCameraRoll, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToFlickr, UIActivity.ActivityType.postToVimeo]
        let prepareMainText = PrepareMessageHeader(title: Share.title, subject: Share.subject, rawURL: rawUrl)
        let prepareURL = PrepareMessageContent(raw: rawUrl)
        let ac = UIActivityViewController(activityItems: [Share.title], applicationActivities: nil)
        ac.excludedActivityTypes = excludeActivity
        ac.setValue(Share.subject, forKey: "subject")
        //if let popoverController = ac.popoverPresentationController {
        //    popoverController.barButtonItem = sender
        //}
        present(ac, animated: true, completion: nil)
    }

    @IBAction func Animate(_ sender: Any) {
        /*if let annotation = map?.annotations?[0],
            let viewAnnotation = map?.view(for: annotation) {
            let pop = PopView()
            pop.center = CGPoint(x: viewAnnotation.bounds.origin.x/2 + 20, y: viewAnnotation.bounds.origin.y/2 + 20)
            viewAnnotation.addSubview(pop)
        } */
    }

    @IBAction func onEdit(_ sender: UITextField) {
        print("value!", sender.text ?? "")
        let input: String = sender.text ?? ""
        let matched = input.match(pattern: "^([0-9]{0,7})|([0-9]{0,7})(\\.[0-9]+)$")
        let valid = input.match(pattern: "(^[0-9]{0,7}$)|(^[0-9]{0,7}\\.[0-9]{1,2}$)")
        print("matched!", matched)
        if valid.isEmpty {
           print("invalid!")
        } else {
            if let validDouble = Double(input) {
                print("validDouble", validDouble)
            }
        }
    }
    
}

public class PrepareMessageHeader : UIActivityItemProvider {
    public let title: String
    public let subject: String
    public let rawURL: String
    public init(title: String, subject: String, rawURL: String) {
        self.title = title
        self.subject = subject
        self.rawURL = rawURL
        super.init(placeholderItem: "")
    }
    public override var item: Any { // customise the item for the current `activityType`
        let targetBundleId = activityType?.rawValue ?? ""

        // check mail apps!
        let mailApps = [
            "com.google.Gmail.ShareExtension"
        ]
        if mailApps.contains(targetBundleId) {
            return self.subject
        }

        // check messenger app
        let shareApps = ["com.facebook.Messenger.ShareExtension"]
        if shareApps.contains(targetBundleId) {
            return "\(self.title) \(self.rawURL)"
        }

        return self.title
    }
}

public class PrepareMessageContent: UIActivityItemProvider {
    public let raw: String
    public init(raw: String) {
        self.raw = raw
        super.init(placeholderItem: raw)
    }
    public override var item: Any { // customise the item for the current `activityType`
        let targetBundleId = activityType?.rawValue ?? ""
        print("PrepareByShareBody", targetBundleId)

        // check mail apps!
        let chatApps = [
            "com.google.hangouts.ShareExtension"
        ]
        if chatApps.contains(activityType?.rawValue ?? "") {
            return self.raw
        }

        // check messenger app
        let shareApps = ["com.facebook.Messenger.ShareExtension"]
        if shareApps.contains(targetBundleId) {
            return ""
        }
        return URL(string: raw)!
    }
}


class ShareWithSubject: NSObject, UIActivityItemSource {
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return Share.title
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        var url = "https://urbania.pe"
        url += "/aviso-n"
        return URL(string: url)
    }

    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return Share.subject
    }
}
