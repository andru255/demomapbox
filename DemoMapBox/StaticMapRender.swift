//
//  StaticMapRender.swift
//  DemoMapBox
//
//  Created by andru on 9/27/18.
//  Copyright © 2018 El Comercio. All rights reserved.
//

import UIKit
import MapboxStatic

extension UIColor {
    var hexString: String {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0
        getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        let rgb: Int = (Int)(red * 255) << 16 | (Int)(green * 255) << 8 | (Int)(blue * 255) << 0
        return String(format: "#%06x", rgb)
    }
}

class StaticMapRender {
    private var view: UIView!
    private var targetImage: UIImageView!
    private var position: CLLocationCoordinate2D!
    private var type: Int!
    private var styleURL: URL? = URL(string: "mapbox://styles/mapbox/streets-v9")
    private var camera: SnapshotCamera!
    
    enum LocationType {
        case point
        case aprox
        case none
    }
    
    init(view: UIView, position: CLLocationCoordinate2D, type: Int = 0) {
        self.view = view
        self.position = position
        self.type = type
        self.camera = SnapshotCamera(lookingAtCenter: self.position, zoomLevel: 16)
    }
    
    func checkLocation(with type: Int) -> LocationType {
        switch type {
        case 1: return .point
        case 2: return .aprox
        default: return .none
        }
    }

    func render() {
        switch checkLocation(with: self.type) {
        case .point:
            createAndAppendImageView()
            let options = SnapshotOptions(styleURL: styleURL!, camera: camera, size: targetImage.bounds.size)
            //options.overlays = [defaultMarker()]
            //options.overlays = [customMarker()]
            appendCustomMarker()
            appendStaticImage(options: options)
        case .aprox:
            createAndAppendImageView()
            let options = SnapshotOptions(styleURL: styleURL!, camera: camera, size: targetImage.bounds.size)
            let circleCoords = circlePath(center: self.position, distance: 50)
            let circle = Path(coordinates: circleCoords)
            circle.fillColor = UIColor.gray.withAlphaComponent(0.25)
            options.overlays = [circle]
            appendStaticImage(options: options)
        default:
            return
        }
    }

    private func createAndAppendImageView() {
        self.targetImage = UIImageView(frame: self.view.frame)
        targetImage.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        view.addSubview(self.targetImage)
        // constraints
        self.targetImage.translatesAutoresizingMaskIntoConstraints = false
        self.targetImage.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.targetImage.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.targetImage.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.targetImage.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.targetImage.layoutIfNeeded()
    }

    private func appendStaticImage(options: SnapshotOptions) {
        _ = Snapshot(options: options).image { (image, error)  in
            if let existError = error {
                print(existError)
                return
            }
            self.targetImage.image = image
        }
    }

    private func defaultMarker() -> Overlay {
        let colorMarker = #colorLiteral(red: 0.9058823529, green: 0.3921568627, blue: 0.07843137255, alpha: 1).hexString
        let metaData: [String: Any] = [
            "type": "FeatureCollection",
            "features": [[
                "type": "Feature",
                "properties": [
                    "title": "foo",
                    "description": "bar",
                    "marker-color": colorMarker,
                    "marker-size": "medium",
                    "marker-symbol": ""
                ],
                "geometry": [
                    "type": "Point",
                    "coordinates": [self.position.longitude, self.position.latitude]
                ]
            ]]
        ]
        let data = try! JSONSerialization.data(withJSONObject: metaData, options: [])
        let output = String(data: data, encoding: .utf8)!
        print("data", output)
        return try! GeoJSON(object: metaData)
        //return Marker(coordinate: self.position, size: .medium, iconName: "cafe")
        //return Marker(coordinate: self.position, letter: 0x4D)
    }
    
    private func appendCustomMarker() {
        let image = #imageLiteral(resourceName: "ProyectAnnotationMap")
        let iconView = UIImageView(frame: self.targetImage.frame)
        iconView.image = image
        view.addSubview(iconView)
        // constraints
        iconView.translatesAutoresizingMaskIntoConstraints = false
        iconView.centerXAnchor.constraint(equalTo: self.targetImage.centerXAnchor).isActive = true
        iconView.centerYAnchor.constraint(equalTo: self.targetImage.centerYAnchor).isActive = true
        self.targetImage.layoutIfNeeded()
    }
    
    private func circlePath(center: CLLocationCoordinate2D, distance: Double) -> [CLLocationCoordinate2D] {
        var result: [CLLocationCoordinate2D] = []
        let steps = 64
        let metters = distance / 1000
        let distanceX = metters / (111.320 * cos(center.latitude * Double.pi/180))
        let distanceY = metters / 110.574
        for angle in 0..<steps {
            let radianAngle = Double(angle)/Double(steps) * (2 * Double.pi)
            let x = center.longitude + (distanceX * cos(radianAngle))
            let y = center.latitude + (distanceY * sin(radianAngle))
            result.append(CLLocationCoordinate2D(latitude: y, longitude: x))
        }
        result.append(result[0])
        return result
    }
}
